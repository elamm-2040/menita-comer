import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {LandingComponent} from "./landing.component";
import {GeneralLayoutComponent} from "../../core/components/layouts/general-layout/general-layout.component";
import {PostViewComponent} from "./post-view/post-view.component";

const router: Routes = [
  {
    path: '',
    component: GeneralLayoutComponent,
    children: [
      {
        path: '',
        component: LandingComponent
      }
    ],
    data: {
      top: false
    }
  },
  {
    path: 'post',
    component: GeneralLayoutComponent,
    children: [
      {
        path: ':id',
        component: PostViewComponent
      }
    ],
    data: {
      top: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(router)],
  exports: [RouterModule]
})
export class LandingRouting {
}
