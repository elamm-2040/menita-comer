import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {PostService} from "../../../core/services/post.service";
import {Post} from "../../../core/models/post";
import {Comments} from "../../../core/models/comments";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import swal from "sweetalert2";

@Component({
  selector: 'app-post-view',
  templateUrl: './post-view.component.html',
  styleUrls: ['./post-view.component.scss']
})
export class PostViewComponent implements OnInit {
  posts: Post;
  comments: Array<Comments> = [];
  loading: boolean = false;
  ngForm: FormGroup;
  id: Number;
  login = false;
  edit = false;
  editId = 0;

  toast = swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3500
  });

  constructor(private router: ActivatedRoute,
              private rest: PostService,
              private formBuilder: FormBuilder) {
    if (localStorage.getItem('login') == 'true') {
      this.login = true;
    }
    this.id = parseInt(router.snapshot.paramMap.get('id'), 10);
    rest.getPost(this.id).subscribe((data) => {
      this.posts = data;
      rest.getPostComments(this.id).subscribe((data) => {
        this.comments = data;
        this.loading = true;
      });
    });
    this.ngForm = this.createForm();
  }

  ngOnInit(): void {
  }

  createForm() {
    return this.formBuilder.group({
      body: ['',
        [Validators.required]],
      email: ['',
        [Validators.email,
         Validators.required]],
      name: ['',
        [Validators.required]],
      postId: [this.id]
    });
  }

  sendComment() {
    this.loading = false;
    if (!this.edit) {
      this.rest.sendPostComments(this.id, this.ngForm.value).subscribe((data) => {
        this.comments.unshift(data);
        this.toast.fire({
          icon: 'success',
          title: 'Comment added'
        });
        this.loading = true;
      });
    } else {
      for (let cont = 0; cont < this.comments.length; cont++) {
        if (this.comments[cont].id == this.editId) {
          this.comments[cont].body = this.ngForm.get('body').value;
          this.comments[cont].email = this.ngForm.get('email').value;
          this.comments[cont].name = this.ngForm.get('name').value;
          this.editId = 0;
        }
      }
      this.toast.fire({
        icon: 'success',
        title: 'Comment edited'
      });
      this.edit = false;
      this.loading = true;
    }
  }

  logged(event) {
    this.login = event;
  }

  delete(id) {
    /*this.rest.deletePostComments(this.id, id).subscribe((data) => {
     console.log(data);
     });*/
    for (let cont = 0; cont < this.comments.length; cont++) {
      if (this.comments[cont].id == id) {
        this.comments.splice(cont, 1);
        this.toast.fire({
          icon: 'success',
          title: 'Comment deleted'
        });
      }
    }
  }

  editComment(id) {
    this.edit = true;
    this.editId = id;
    for (let cont = 0; cont < this.comments.length; cont++) {
      if (this.comments[cont].id == id) {
        this.ngForm.patchValue(this.comments[cont]);
      }
    }
  }

}
