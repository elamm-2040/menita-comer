import {Component, OnInit} from '@angular/core';
import {PostService} from "../../core/services/post.service";
import {Post} from "../../core/models/post";
import {$} from "protractor";

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
  postsOrig: Array<Post> = [];
  posts: Array<Post> = [];
  curPage: number = 1;
  pageSize: number = 10;
  pageIndex: number = 0;
  filtersLoaded: Promise<boolean>;

  constructor(private rest: PostService) {
    rest.getAllPost().subscribe(data => {
      this.posts = data;
      this.postsOrig = JSON.parse(JSON.stringify(data));
      this.filtersLoaded = Promise.resolve(true);
    });
  }

  ngOnInit(): void {
  }

  numberOfPages() {
    return this.posts.length;
  };

  pageEvent(event) {
    if (event.pageIndex > this.pageIndex) {
      this.pageIndex++;
      this.curPage++;
    } else {
      this.pageIndex--;
      this.curPage--;
    }
  }

  search(event) {
    this.posts = event;
  }

}
