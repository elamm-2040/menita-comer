import {NgModule} from "@angular/core";
import {CommonModule} from '@angular/common';
import {LandingComponent} from "./landing.component";
import {LandingRouting} from "./landing.routing";
import {GeneralLayoutComponent} from "../../core/components/layouts/general-layout/general-layout.component";
import {SharedModule} from "../../core/modules/shared.module";
import {PostService} from "../../core/services/post.service";
import { PostViewComponent } from './post-view/post-view.component';

@NgModule({
  declarations: [
    LandingComponent,
    GeneralLayoutComponent,
    PostViewComponent
  ],
  imports: [
    CommonModule,
    LandingRouting,
    SharedModule
  ],
  providers: [
    PostService
  ]
})
export class LandingModule {
}
