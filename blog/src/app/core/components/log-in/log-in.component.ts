import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import swal from "sweetalert2";
import {MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss']
})
export class LogInComponent implements OnInit {
  ngForm: FormGroup;
  toast = swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3500
  });

  constructor(private formBuilder: FormBuilder,
              private dialogRef: MatDialogRef<LogInComponent>) {
    this.ngForm = this.createForm();
  }

  ngOnInit(): void {
  }

  createForm() {
    return this.formBuilder.group({
      password: ['',
        [Validators.required]],
      email: ['',
        [Validators.email,
         Validators.required]],
    });
  }

  login() {
    if (this.ngForm.get('email').value == 'admin@admin.com' && this.ngForm.get('password').value == 'admin') {
      localStorage.setItem('login', 'true');
      this.toast.fire({
        icon: 'success',
        title: 'Welcome Admin'
      });
      this.dialogRef.close(true);
    } else {
      this.toast.fire({
        icon: 'error',
        title: 'Unregistered user'
      });
      this.dialogRef.close(false);
    }
  }

}
