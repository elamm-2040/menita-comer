import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Post} from "../../models/post";
import {MatDialog} from "@angular/material/dialog";
import {LogInComponent} from "../log-in/log-in.component";
import swal from "sweetalert2";

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
  @Input() postsOrig: Array<Post>;
  @Input() searchInput: boolean;
  @Output() posts: EventEmitter<any> = new EventEmitter<any>();
  @Output() logged: EventEmitter<any> = new EventEmitter<any>();
  searchValue: String = '';
  login = false;

  toast = swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3500
  });

  constructor(private dialog: MatDialog) {
    if (localStorage.getItem('login') == 'true') {
      this.login = true;
    }
  }

  ngOnInit(): void {
  }

  search() {
    this.posts.emit(this.postsOrig.filter((i) => {
      return i.title.includes(this.searchValue.toString());
    }));
  }

  openDialog() {
    const dialogRef = this.dialog.open(LogInComponent);

    dialogRef.afterClosed().subscribe(result => {
      this.login = result;
      this.logged.emit(result);
    })
  }

  logout() {
    this.toast.fire({
      icon: 'success',
      title: 'See ya!'
    });

    localStorage.clear();
    this.login = false;
    this.logged.emit(false);
  }

}
