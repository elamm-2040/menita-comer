import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from "@angular/common/http";
import {MaterialModule} from "./material.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NavBarComponent} from "../components/nav-bar/nav-bar.component";
import {RouterModule} from "@angular/router";
import {LogInComponent} from "../components/log-in/log-in.component";

@NgModule({
  declarations: [
    NavBarComponent,
    LogInComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    MaterialModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule
  ],
  exports: [
    MaterialModule,
    FormsModule,
    NavBarComponent,
    ReactiveFormsModule,
    LogInComponent
  ]
})
export class SharedModule {
}
