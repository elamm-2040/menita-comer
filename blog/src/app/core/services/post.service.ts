import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) {
  }

  getAllPost(): Observable<any> {
    return this.http.get(`https://jsonplaceholder.typicode.com/posts`);
  }

  getPost(id): Observable<any> {
    return this.http.get(`https://jsonplaceholder.typicode.com/posts/${id}`);
  }

  getPostComments(id): Observable<any> {
    return this.http.get(`https://jsonplaceholder.typicode.com/posts/${id}/comments`);
  }

  sendPostComments(id, data): Observable<any> {
    return this.http.post(`https://jsonplaceholder.typicode.com/posts/${id}/comments`, data);
  }

  deletePostComments(id, comment): Observable<any> {
    return this.http.delete(`https://jsonplaceholder.typicode.com/posts/${id}/comments/${comment}`);
  }

}
